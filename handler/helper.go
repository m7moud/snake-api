package handler

import (
	"encoding/json"
	"net/http"
)

type errorResponse struct {
	Message string `schema:"message"`
}

// JSONError responds with a JSON error.
func JSONError(w http.ResponseWriter, error string, code int) {
	data := errorResponse{
		Message: error,
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(data)
}
