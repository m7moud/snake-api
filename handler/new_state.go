package handler

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/gorilla/schema"
	"github.com/sumup-challenges/backend-challenge-template-m7moud/snake"
)

// StateInput wraps the input params for creating new state.
type StateInput struct {
	Width  int `schema:"w"`
	Height int `schema:"h"`
}

// NewState responds with a new game state.
func NewState(w http.ResponseWriter, r *http.Request) {
	input, err := parseNewInput(r)
	if err != nil {
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	data := snake.NewState(input.Width, input.Height)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(data)
}

func parseNewInput(r *http.Request) (*StateInput, error) {
	if len(r.URL.Query()) != 2 {
		return nil, errors.New("Parameters count doesn't match")
	}

	input := new(StateInput)
	if err := schema.NewDecoder().Decode(input, r.URL.Query()); err != nil {
		return nil, err
	}

	return input, nil
}
