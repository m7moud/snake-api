package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/sumup-challenges/backend-challenge-template-m7moud/snake"
)

// ValidateInput wraps the input for state validation.
type ValidateInput struct {
	*snake.State
	Ticks *[]snake.Tick `json:"ticks"`
}

// ValidateState validates game state.
func ValidateState(w http.ResponseWriter, r *http.Request) {
	input, err := parseValidateInput(r)
	if err != nil {
		JSONError(w, err.Error(), http.StatusBadRequest)
		return
	}

	data, err := input.State.Validate(*input.Ticks)
	if err != nil {
		switch {
		case errors.Is(err, snake.ErrFruitNotFound):
			JSONError(w, err.Error(), http.StatusNotFound)
		case errors.Is(err, snake.ErrGameOver):
			JSONError(w, err.Error(), http.StatusTeapot)
		}
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(data)
}

func parseValidateInput(r *http.Request) (*ValidateInput, error) {
	var input ValidateInput
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()
	err := dec.Decode(&input)
	if err != nil {
		return nil, fmt.Errorf("Error parsing body: %v", err)
	}

	if input.Ticks == nil {
		return nil, errors.New("ticks are missing")
	}

	return &input, nil
}
