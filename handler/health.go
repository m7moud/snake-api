package handler

import (
	"net/http"
)

// Health responds with OK.
func Health(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
