FROM golang:latest

WORKDIR /go/src/github.com/sumup-challenges/backend-challenge-template-m7moud/

RUN apt-get update && \
    apt-get install -y build-essential

COPY . .

RUN go get -d -v ./...

RUN go install -v ./...

RUN go build -o snake

RUN go build -o ./out/snake .

EXPOSE 8080

CMD ["./out/snake"]

