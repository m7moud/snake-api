package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/sumup-challenges/backend-challenge-template-m7moud/handler"
)

var (
	appPort = 8080
)

// App represents an app server.
type App struct {
	Port    int
	Handler http.Handler
}

// New initiates a new app server.
func New() *App {
	return &App{
		Port: appPort,
	}
}

// Start starts a server.
func (a *App) Start() {
	var wait time.Duration
	addr := fmt.Sprintf(":%d", a.Port)
	log.Printf("Starting app on %s", addr)

	srv := &http.Server{
		Addr:         addr,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      a.Handler,
	}

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)

}

// RegisterRoutes registers app routes.
func (a *App) RegisterRoutes() {
	r := mux.NewRouter()
	r.HandleFunc("/", handler.Health).Methods("GET")
	r.HandleFunc("/new", handler.NewState).Methods("GET")
	r.HandleFunc("/validate", handler.ValidateState).Methods("POST")

	a.Handler = r
}

// RegisterMiddleware registers app middleware.
func (a *App) RegisterMiddleware() {
	h := a.Handler
	h = handlers.CompressHandler(h)
	h = handlers.RecoveryHandler()(h)
	h = handlers.LoggingHandler(os.Stdout, h)

	a.Handler = h
}
