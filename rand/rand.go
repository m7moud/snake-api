package rand

import "math/rand"

var letters = []rune("_-0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

// IntExcluding returns, as an int, a non-negative pseudo-random number in [0,n)
// If end is zero it will return 0
// If end is 1 it will return 0 or 1
// Can exclude certain integers.
func IntExcluding(end int, excludes ...int) int {
	if end == 0 {
		return 0
	}

	r := rand.Intn(end)

	if end == 1 {
		return r
	}

Loop:
	for {
		for _, exclude := range excludes {
			if r == exclude {
				r = rand.Intn(end)
				continue Loop
			}
			break
		}
		break
	}

	return r
}

// Seq returns a random string with a given length.
func Seq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	return string(b)
}
