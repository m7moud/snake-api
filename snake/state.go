package snake

import (
	"github.com/sumup-challenges/backend-challenge-template-m7moud/rand"
)

// State represents the state of the game.
type State struct {
	GameID string `json:"gameId"`
	Width  int    `json:"width"`
	Height int    `json:"height"`
	Score  int    `json:"score"`
	Fruit  fruit  `json:"fruit"`
	Snake  snake  `json:"snake"`
}

type fruit struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type snake struct {
	X    int `json:"x"`
	Y    int `json:"y"`
	VelX int `json:"velX"` // X velocity of the snake (-1, 0, 1)
	VelY int `json:"velY"` // Y velocity of the snake (-1, 0, 1)
}

// NewState initiates a new game state.
func NewState(w int, h int) *State {
	return State{
		Width:  w,
		Height: h,
	}.init()
}

func (s State) init() *State {
	snake := snake{
		VelX: 1,
	}

	s.Snake = snake
	s.GameID = rand.Seq(12)
	s.newFruit()

	return &s
}

func (s *State) newFruit() {
	fruit := new(fruit)
	fruit.X = rand.IntExcluding(s.Width, s.Fruit.X)
	fruit.Y = rand.IntExcluding(s.Height, s.Fruit.Y)

	s.Fruit = *fruit
}
