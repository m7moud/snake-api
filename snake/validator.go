package snake

import (
	"errors"
)

// Tick represents a game moves.
type Tick struct {
	VelX int `json:"velX"`
	VelY int `json:"velY"`
}

// ErrFruitNotFound ticks do not lead the snake to the fruit position.
var ErrFruitNotFound = errors.New("Fruit not found")

// ErrGameOver snake went out of bounds or made an invalid move.
var ErrGameOver = errors.New("Game over")

type validator struct {
	state *State
	err   error
}

// Validate checks if an array of moves are valid or not
// Returns an error if the moves are invalid.
// Increments Score and generates a new fruit position if the moves lead the snake to the fruit.
func (s State) Validate(ticks []Tick) (*State, error) {
	v := validator{state: &s}
	v.validMoves(ticks)
	v.validFruit()
	if v.err != nil {
		return nil, v.err
	}

	s.Score++
	s.newFruit()

	return &s, nil
}

func (v *validator) validMoves(t []Tick) bool {
	if v.err != nil {
		return false
	}

	lastTick := Tick{}
	for _, tick := range t {
		v.state.Snake.X += tick.VelX
		v.state.Snake.Y += tick.VelY

		if tick.isDiagonalMove() || tick.isBackwardsMove(lastTick) || v.state.isOutOfBoundaries() {
			v.err = ErrGameOver
			return false
		}

		lastTick = tick
	}

	// Update snake state
	v.state.Snake.VelX = lastTick.VelX
	v.state.Snake.VelY = lastTick.VelY

	return true
}

func (t Tick) isDiagonalMove() bool {
	return (t.VelX != 0 && (t.VelY > 0 || t.VelY < 0)) || (t.VelY != 0 && (t.VelX > 0 || t.VelX < 0))
}

func (t Tick) isBackwardsMove(lastTick Tick) bool {
	return (t.VelX == 0 && lastTick.VelY > 0 && t.VelY < 0) || (t.VelY == 0 && lastTick.VelX > 0 && t.VelX < 0)
}

func (s State) isOutOfBoundaries() bool {
	return s.Snake.X > s.Width || s.Snake.Y > s.Height || s.Snake.X < 0 || s.Snake.Y < 0
}

func (v *validator) validFruit() bool {
	if v.err != nil {
		return false
	}

	if v.state.Snake.X != v.state.Fruit.X || v.state.Snake.Y != v.state.Fruit.Y {
		v.err = ErrFruitNotFound
		return false
	}

	return true
}
