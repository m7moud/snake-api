package snake_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"github.com/sumup-challenges/backend-challenge-template-m7moud/snake"
)

func TestSnake(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Snake Suite")
}

var _ = Describe("Snake", func() {
	Describe("Creating new game state", func() {
		Context("With 0 for width and height", func() {
			state := snake.NewState(0, 0)
			It("should have width and height of 0", func() {

				Expect(state.Width).To(BeZero())
				Expect(state.Height).To(BeZero())
			})

			It("should have a fruit at position 0", func() {
				Expect(state.Fruit.X).To(BeZero())
				Expect(state.Fruit.Y).To(BeZero())
			})
		})

		Context("With 10 for width and height", func() {
			state := snake.NewState(10, 10)
			state2 := snake.NewState(10, 10)

			It("should have width and height of 10", func() {
				Expect(state.Width).To(Equal(10))
				Expect(state.Height).To(Equal(10))
			})

			It("should have a fruit at random position > 0", func() {
				Expect(state.Fruit.X).To(BeNumerically(">", 0))
				Expect(state.Fruit.Y).To(BeNumerically(">", 0))

				Expect(state2.Fruit.X).To(Not(Equal(state.Fruit.X)))
				Expect(state2.Fruit.Y).To(Not(Equal(state.Fruit.Y)))
			})

			It("should have a random string GameID", func() {
				Expect(len(state.GameID)).To(BeNumerically(">", 0))
				Expect(state2.GameID).To(Not(Equal(state.GameID)))
			})

			It("should start with snake velocity 1, 0", func() {
				Expect(state.Snake.VelX).To(Equal(1))
				Expect(state.Snake.VelY).To(Equal(0))
			})
		})
	})

	Describe("Validator", func() {
		Context("With valid ticks", func() {
			state := snake.NewState(5, 5)
			state.Fruit.X = 4
			state.Fruit.Y = 4
			oldFruit := state.Fruit
			oldScore := state.Score

			state, err := state.Validate([]snake.Tick{
				{VelX: 1, VelY: 0},
				{VelX: 1, VelY: 0},
				{VelX: 1, VelY: 0},
				{VelX: 0, VelY: 1},
				{VelX: 0, VelY: 1},
				{VelX: 0, VelY: 1},
				{VelX: 1, VelY: 0},
				{VelX: 0, VelY: 1},
			})

			It("should be valid", func() {
				Expect(err).ToNot(HaveOccurred())
			})

			It("should generate a new fruit", func() {
				Expect(state.Fruit).ToNot(Equal(oldFruit))
			})

			It("should increment score", func() {
				Expect(state.Score).To(Equal(oldScore + 1))
			})
		})
	})
})
