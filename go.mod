module github.com/sumup-challenges/backend-challenge-template-m7moud

go 1.16

require (
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/onsi/ginkgo v1.16.1
	github.com/onsi/gomega v1.11.0
)
