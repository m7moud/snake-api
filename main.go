package main

import (
	"math/rand"
	"time"

	"github.com/sumup-challenges/backend-challenge-template-m7moud/app"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	app := app.New()
	app.RegisterRoutes()
	app.RegisterMiddleware()
	app.Start()
}
